FROM gitlab-registry.cern.ch/industrial-controls/sw-infra/jenkins/master-master:2.303.4

# Install FESA specific plugins, e.g. :
USER root
RUN jenkins-plugin-cli --plugins ssh-slaves blueocean pipeline-utility-steps

USER 1001

ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["/usr/libexec/run"]

